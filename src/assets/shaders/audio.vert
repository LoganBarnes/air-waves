#version 100
precision highp float;
precision highp int;

attribute vec2 texCoords;

uniform mat4 screenFromWorld;
uniform mat4 worldFromLocal;

const float PI = 3.1415926536;

const int degree = 2;
const int numPoints = 25;
const int order = degree + 1;
const int numSpans = numPoints - degree;

uniform float fft[32]; // won't be sending anything larger than 128
uniform float minDb;

int getSpan(in float t)
{
    float u = t * float(numSpans);
    float uFloor = floor(u);
    int span = int(uFloor) + degree;
    return span;
}

void main() {
  int span = getSpan(1.0 - texCoords.x);

  float angle = texCoords.x * PI * 2.0;
  float r = max(0.0, (fft[span - degree + 1]) * 0.005);

  vec2 p = vec2(-cos(angle), -sin(angle)) * r;
  gl_Position = screenFromWorld * worldFromLocal * vec4(p, 0.0, 1.0);
}
