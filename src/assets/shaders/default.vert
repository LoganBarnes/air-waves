#version 100
precision highp float;
precision highp int;

attribute vec2 localPosition;

uniform mat4 screenFromWorld;
uniform mat4 worldFromLocal;

void main() {
  gl_Position = screenFromWorld * worldFromLocal * vec4(localPosition, 0.0, 1.0);
}
