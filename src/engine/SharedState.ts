import { Camera, CameraMover } from "ts-graphics";
import GameRunner from "./SimRunner";

class SharedState {
  private _paused: boolean;
  worldTime: number;
  renderAlpha: number;
  timeStep: number;
  cameraMover: CameraMover;

  private runner: GameRunner;

  constructor(
    paused_: boolean,
    worldTime_: number,
    renderAlpha_: number,
    runner: GameRunner,
    timeStep: number = 1.0 / 60.0
  ) {
    this._paused = paused_;
    this.worldTime = worldTime_;
    this.renderAlpha = renderAlpha_;
    this.timeStep = timeStep;
    this.cameraMover = new CameraMover(new Camera());
    this.runner = runner;
  }

  get paused(): boolean {
    return this._paused;
  }

  set paused(paused: boolean) {
    this._paused = paused;
    this.runner.runLoop();
  }
}

export default SharedState;
