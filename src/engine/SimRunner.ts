import SharedState from "./SharedState";

class SimRunner {
  private prevTimeMillis: number;
  private accumulator: number;
  private _sharedState: SharedState;

  private _updateFunction: (_sharedState: SharedState) => void;
  private _renderFunction: (_sharedState: SharedState) => void;

  constructor(
    updateFunction: (_sharedState?: SharedState) => void,
    renderFunction: (_sharedState?: SharedState) => void
  ) {
    this.prevTimeMillis = 0;
    this.accumulator = 0;
    this._sharedState = new SharedState(false, 0, 0, this);
    this._updateFunction = updateFunction;
    this._renderFunction = renderFunction;
  }

  // time is assumed to be in seconds unless specified
  loop(newTimeMillis: number): void {
    const timeDiff = (newTimeMillis - this.prevTimeMillis) * 1e-3; // ms to s
    const frameTime: number = Math.min(timeDiff, 0.1);
    this.prevTimeMillis = newTimeMillis;

    if (!this._sharedState.paused) {
      this.accumulator += frameTime;

      while (this.accumulator >= this._sharedState.timeStep) {
        this._sharedState.worldTime += this._sharedState.timeStep;
        this.accumulator -= this._sharedState.timeStep;
        this._updateFunction(this._sharedState);
      }
    }

    this._sharedState.renderAlpha =
      this.accumulator / this._sharedState.timeStep;
    this._renderFunction(this._sharedState);

    if (!this._sharedState.paused) {
      window.requestAnimationFrame(this.loop.bind(this));
    }
  }

  runLoop() {
    if (!this._sharedState.paused) {
      this.prevTimeMillis = window.performance.now();
      this.loop(window.performance.now());
    }
  }

  get sharedState(): SharedState {
    return this._sharedState;
  }

  set updateFunction(updateFunction: (_sharedState?: SharedState) => void) {
    this._updateFunction = updateFunction;
  }

  set renderFunction(renderFunction: (_sharedState?: SharedState) => void) {
    this._renderFunction = renderFunction;
  }
}

export default SimRunner;
