class ALUtils {
  private _context: AudioContext;
  private buffers: AudioBuffer[];
  private bufferSourceNodes: AudioBufferSourceNode[];
  private analyser: AnalyserNode;
  private _fftDataByte: Uint8Array;
  private _fftDataFloat: Float32Array;
  private zeros: Float32Array;

  constructor() {
    this._context = new window.AudioContext();
    this.buffers = [];
    this.bufferSourceNodes = [];
    this.analyser = this._context.createAnalyser();

    this.analyser.fftSize = 64;
    this.analyser.connect(this.context.destination);

    const arrayLength: number = this.analyser.frequencyBinCount;

    this._fftDataByte = new Uint8Array(arrayLength);
    this._fftDataFloat = new Float32Array(arrayLength);
    this.zeros = new Float32Array(arrayLength);
    this.zeros.fill(0);

    const al: ALUtils = this;

    const soundData: ArrayBuffer[] = [
      require("@/assets/audio/ogg/bass.ogg"),
      require("@/assets/audio/ogg/bells.ogg"),
      require("@/assets/audio/ogg/drums.ogg"),
      require("@/assets/audio/ogg/guitar.ogg"),
      require("@/assets/audio/ogg/piano.ogg")
    ];

    soundData.forEach((data: ArrayBuffer) => {
      this._context.decodeAudioData(data, (buffer: AudioBuffer) => {
        al.buffers.push(buffer);
      });
    });
  }

  pause(): void {
    this.bufferSourceNodes.forEach((node: AudioBufferSourceNode) => {
      node.stop();
    });
    this.bufferSourceNodes = [];
  }

  resume(): void {
    this.pause();
    this.buffers.forEach((buffer: AudioBuffer) => {
      const bufferSourceNode: AudioBufferSourceNode = this._context.createBufferSource();
      bufferSourceNode.buffer = buffer;
      bufferSourceNode.connect(this.analyser);
      this.bufferSourceNodes.push(bufferSourceNode);
    });
    this.bufferSourceNodes.forEach((node: AudioBufferSourceNode) => {
      node.start();
    });
  }

  get context(): AudioContext {
    return this._context;
  }

  get fftDataByte(): Uint8Array {
    if (this.bufferSourceNodes.length == 0) {
      return this.zeros;
    }
    this.analyser.getByteFrequencyData(this._fftDataByte);
    return this._fftDataByte;
  }

  get fftDataFloat(): Float32Array {
    if (this.bufferSourceNodes.length == 0) {
      return this.zeros;
    }
    this.analyser.getFloatFrequencyData(this._fftDataFloat);
    return this._fftDataFloat;
  }

  get minDb(): number {
    if (this.bufferSourceNodes.length == 0) {
      return 0;
    }
    return this.analyser.minDecibels;
  }
}

export default ALUtils;
