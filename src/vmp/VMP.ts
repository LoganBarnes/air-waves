import { vec3, mat4 } from "gl-matrix";
import audioVert from "ts-shader-loader!@/assets/shaders/audio.vert";
import defaultFrag from "ts-shader-loader!@/assets/shaders/default.frag";
import { GLUtils, GLProgram, GLBuffer, GLVertexArray } from "ts-graphics";
import SharedState from "@/engine/SharedState";
import ALUtils from "@/ts-audio/ALUtils";

class VMP {
  private glUtils: GLUtils;
  private alUtils: ALUtils;
  private program: GLProgram;
  private vbo: GLBuffer;
  private vao: GLVertexArray;
  private modelMatrix: mat4;
  private color: vec3;
  private prevFftData: Uint8Array;
  private currFftData: Uint8Array;
  private fftData: Float32Array;

  constructor(glUtils: GLUtils) {
    this.glUtils = glUtils;
    this.alUtils = new ALUtils();
    this.modelMatrix = mat4.create();

    const gl = this.glUtils.getContext();

    this.program = this.glUtils.createProgram(
      {
        // text: defaultVert,
        text: audioVert,
        type: gl.VERTEX_SHADER
      },
      {
        text: defaultFrag,
        type: gl.FRAGMENT_SHADER
      }
    );

    const data: number[] = [];

    for (let i: number = 0; i <= 128; ++i) {
      data.push(i / 128.0); // x
      data.push(0.0); // y
    }

    this.vbo = this.glUtils.createVbo(new Float32Array(data));

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "texCoords",
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ]);

    this.color = vec3.fromValues(1.0, 0.5, 0.1); // orange

    this.prevFftData = this.alUtils.fftDataByte;
    this.currFftData = this.prevFftData;
    this.fftData = Float32Array.from(this.currFftData);
  }

  update(state: SharedState): void {
    this.prevFftData = this.currFftData;
    this.currFftData = this.alUtils.fftDataByte;
  }

  render(state: SharedState): void {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    const t: number = state.renderAlpha;
    const invT: number = 1.0 - t;

    this.fftData.forEach((_: number, index: number, array: Float32Array) => {
      array[index] =
        this.prevFftData[index] * invT + this.currFftData[index] * t;
    });

    this.program.use(
      (): void => {
        this.program.setMatrixUniform(
          state.cameraMover.camera.screenFromWorldMatrix,
          "screenFromWorld"
        );

        this.program.setMatrixUniform(this.modelMatrix, "worldFromLocal");

        gl.uniform1fv(
          gl.getUniformLocation(this.program.glId, "fft"),
          this.fftData
        );

        this.program.setFloatUniform(this.color, "color");
        this.vao.render(gl.TRIANGLE_FAN, 0, 129);
      }
    );
  }

  set paused(paused: boolean) {
    if (paused) {
      this.alUtils.pause();
    } else {
      this.alUtils.resume();
    }
  }
}

export default VMP;
